import faker from 'faker/locale/tr';

export const getRandomProduct = () => {
  return {
    productName: faker.commerce.productName(),
    oldPrice: parseFloat(faker.commerce.price(100, 300)),
    newPrice: parseFloat(faker.commerce.price(50, 100)),
    imageUrl: faker.image.fashion(540, 757),
    isLiked: false,
  };
};
