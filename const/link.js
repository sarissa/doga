export const GENERATE_PRODUCT_DETAIL_LINK = ({ id, productName }) => `/u/${id}/${productName}`;
